<?php

/**
 * @file
 *
 * Implementation of Embedded Media Field hooks.
 */

define('EMVIDEO_GOOGLEDOCS_MAIN_URL', 'https://docs.google.com/');

/**
 * Implementation of <emmodule>_<provider>_info().
 */
function emvideo_google_presentations_info() {
  return array(
    'provider' => 'google_presentations',
    'name' => t('Googledocs Presentation'),
    'url' => EMVIDEO_GOOGLEDOCS_MAIN_URL,
    'settings_description' => t(
      'These settings specifically affect videos displayed from <a href="@slideshare" target="_blank">Googledocs Presentation</a>.',
      array('@slideshare' => EMVIDEO_GOOGLEDOCS_MAIN_URL)
    ),
    'supported_features' => array(
      array(t('Autoplay'), t('Yes'), ''),
      array(t('RSS Attachment'), t('No'), ''),
      array(t('Thumbnails'), t('No'), ''),
      array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
    ),
  );
}

/**
 * Parser function for submissions via emvideo.
 *
 * @param $embed
 *   A URL or wordpress embed code for the slideshow to embed.
 * @return
 *   The slideshow_id of the slideshare presentation.
 *
 * TODO: refine the regex used to match urls so it can pull a url out of the regular
 * embed code, or write a condition to pull the slideshow id out of the embed code.
 */
function emvideo_google_presentations_extract($embed) {
  $exp = '/https?:.+docs.google.com\/present\/((view)|(embed))\?[^#.]+/i';
  if (preg_match($exp, check_url($embed), $matches)) { //URL parsing
    return $embed;
  }

  return FALSE;
}

/**
 * Implementation of hook_<provider>_video_link()
 */
function emvideo_google_presentations_video_link($video_code) {
  return NULL;
}

/**
 * Implementation of hook_<provider>_video()
 */
function emvideo_google_presentations_video($embed, $width, $height, $field, $item, &$node, $autoplay) {
  return theme('google_presentations_flash', $embed, $width, $height, $field, $item, $node, $autoplay);
}

/**
 * Implementation of hook_<provider>_preview()
 */
function emvideo_google_presentations_preview($embed, $width, $height, $field, $item, &$node, $autoplay) {
  return theme('google_presentations_flash', $embed, $width, $height, $field, $item, $node, $autoplay);
}

/**
 *  Implement hook_emvideo_PROVIDER_content_generate().
 */
function emvideo_google_presentations_content_generate() {
  return array(
    'https://docs.google.com/present/view?id=dd8nn97m_141hkj5t5gz',
    'https://docs.google.com/present/view?id=dcdmcbvp_28d8v8t9gc',
  );
}
